#!/usr/bin/env bash
# shellcheck disable=SC2034

iso_name="Willy OS"
iso_label="WILLYOS_$(date +%Y%m)"
iso_publisher="Willy <https://gitlab.com/0efb6>"
iso_application="Willyos Linux Live/Rescue CD"
iso_version="$(date +%Y.%m.%d)"
install_dir="arch"
buildmodes=('iso')
bootmodes=('uefi-x64.systemd-boot.esp' 'uefi-x64.systemd-boot.eltorito')
arch="x86_64"
pacman_conf="pacman.conf"
airootfs_image_type="squashfs"
#airootfs_image_tool_options=('-comp' 'xz' '-Xbcj' 'x86' '-b' '1M' '-Xdict-size' '1M') #xz for final image,compress more than zstd
airootfs_image_tool_options=('-comp' 'xz' '-Xbcj' 'x86' '-b' '1M' '-Xdict-size' '1M') #zstd for debugging
file_permissions=(
  ["/etc/shadow"]="0:0:400"
  ["/etc/gshadow"]="0:0:600"
  ["/etc/polkit-1/rules.d"]="0:0:750"
  ["/etc/sudoers.d"]="0:0:750"
  ["/root"]="0:0:750"
  ["/root/.automated_script.sh"]="0:0:755"
  ["/etc/skel/"]="0:0:744"
  ["/usr/local/bin/choose-mirror"]="0:0:755"
  ["/usr/local/bin/Installation_guide"]="0:0:755"
  ["/usr/local/bin/livecd-sound"]="0:0:755"
  ["/usr/local/bin/willyOS-before"]="0:0:755"
  ["/usr/local/bin/willyOS-dm-check"]="0:0:755"
  ["/usr/local/bin/willyOS-final"]="0:0:755"
  ["/usr/local/bin/get-willyOS-scripts"]="0:0:755"
)