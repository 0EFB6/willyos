# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
#[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

if [ "$TERM" = "st-256color" ]; then
        printf %b '\e[40m' '\e[8]' # set default background to color 0 'dracula-bg'
        printf %b '\e[37m' '\e[8]' # set default foreground to color 7 'dracula-fg'
        printf %b '\e]P0282a36'    # redefine 'black'          as 'dracula-bg'
        printf %b '\e]P86272a4'    # redefine 'bright-black'   as 'dracula-comment'
        printf %b '\e]P1ff5555'    # redefine 'red'            as 'dracula-red'
        printf %b '\e]P9ff7777'    # redefine 'bright-red'     as '#ff7777'
        printf %b '\e]P250fa7b'    # redefine 'green'          as 'dracula-green'
        printf %b '\e]PA70fa9b'    # redefine 'bright-green'   as '#70fa9b'
        printf %b '\e]P3f1fa8c'    # redefine 'brown'          as 'dracula-yellow'
        printf %b '\e]PBffb86c'    # redefine 'bright-brown'   as 'dracula-orange'
        printf %b '\e]P4bd93f9'    # redefine 'blue'           as 'dracula-purple'
        printf %b '\e]PCcfa9ff'    # redefine 'bright-blue'    as '#cfa9ff'
        printf %b '\e]P5ff79c6'    # redefine 'magenta'        as 'dracula-pink'
        printf %b '\e]PDff88e8'    # redefine 'bright-magenta' as '#ff88e8'
        printf %b '\e]P68be9fd'    # redefine 'cyan'           as 'dracula-cyan'
        printf %b '\e]PE97e2ff'    # redefine 'bright-cyan'    as '#97e2ff'
        printf %b '\e]P7f8f8f2'    # redefine 'white'          as 'dracula-fg'
        printf %b '\e]PFffffff'    # redefine 'bright-white'   as '#ffffff'
        clear
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

#PS1='\u\D{%H':'%M':'%S %-d %a %b} \n[\w] $ '
PS1='\[\033[1;31m\]\D{%H':'%M':'%S} \[\033[1;33m\]\D{%-d} \[\033[1;36m\]\D{%a} \[\033[1;32m\]\D{%b} \n\[\033[46m\]\[\033[1;30m\][\w]\[\033[0m\]\$ '
#PS1='\u@\h:\w\$ '
# \u => Username
#\h => Hostname (till :)
#\H => Hostname
#\w => Working Directory
#\a => Bell character
#\j => Number of jobs currently managed by shell
#\e => Escape character
#\d => Date (Weekday Month Date)
#\D{} => Date base on strftime
#\i => Basename of the shell’s terminal device name
#\n => A new LINESe
#\r => A carriage return
#\s => Name of the shell
#\t => Time in 24 hour (HH:MM:SS)
#\T => Time in 12 hour (HH:MM:SS)
#\@ => Time in 12 hour am/pm format
#\A => Time in 24 hour (HH:MM)
#\v => Version of bash
#\V => Version of bash (specific)
#\\ => Blackslash

########## regular colors ##########
#local K="\[\033[0;30m\]"   black
#local R="\[\033[0;31m\]"   red
#local G="\[\033[0;32m\]"   green
#local Y="\[\033[0;33m\]"   yellow
#local B="\[\033[0;34m\]"   blue
#local M="\[\033[0;35m\]"   magenta
#local C="\[\033[0;36m\]"   cyan
#local W="\[\033[0;37m\]"   white
##### emphasized(bolded) colors ####
#local EMK="\[\033[1;30m\]" black
#local EMR="\[\033[1;31m\]" red
#local EMG="\[\033[1;32m\]" green
#local EMY="\[\033[1;33m\]" yellow
#local EMB="\[\033[1;34m\]" blue
#local EMM="\[\033[1;35m\]" magenta
#local EMC="\[\033[1;36m\]" cyan
#local EMW="\[\033[1;37m\]" white
######## background colors#########
#local BGK="\[\033[40m\]"   black
#local BGR="\[\033[41m\]"   red
#local BGG="\[\033[42m\]"   green
#local BGY="\[\033[43m\]"   yellow
#local BGB="\[\033[44m\]"   blue
#local BGM="\[\033[45m\]"   magenta
#local BGC="\[\033[46m\]"   cyan
#local BGW="\[\033[47m\]"   white
#local NONE="\[\033[0m\]"   unsets color to term's fg color

#if [ "$color_prompt" = yes ]; then
#	PS1='\[\033[1;31m\]\D{%H':'%M':'%S} \[\033[1;33m\]\D{%-d} \[\033[1;36m\]\D{%a} \[\033[1;32m\]\D{%b} \n\[\033[46m\]\[\033[1;30m\][\w]\[\033[0m\]\$ '
    #PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;32m\]\w\[\033[00m\]\$ '
#else
#    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
#fi
#unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    #alias fgrep='fgrep --color=auto'
    #alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# See /usr/share/doc/bash-doc/examples in the bash-doc package.
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

export MAKEFLAGS=-j4
export EDITOR=vim
export PATH=$PATH:/root/.local/bin
export PATH=$PATH:/home/linux/.config/rofi/bin
[[ $(fgconsole 2>/dev/null) == 1 ]] && exec startx
